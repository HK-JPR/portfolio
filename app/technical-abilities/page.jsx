import React from 'react'
import TwoSectionCard from '../components/twosectioncard';


async function GetTechnialAbility() {
  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  myHeaders.append("Access-Control-Request-Headers", "*");
  myHeaders.append("run_as_user_id", "660fd2558e68279dc6823674");

  const raw = JSON.stringify({
    "collection": "TechnialAbility",
    "database": "MyApp",
    "dataSource": "Cluster0"
  });

  const requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow"
  };

  return fetch("https://ap-south-1.aws.data.mongodb-api.com/app/data-yvqtb/endpoint/data/v1/action/find", requestOptions)
    .then((response) => response.text())
    .then((result) => JSON.parse(result).documents)
    .catch((error) => console.error(error));
}

export default async function TechAbility() {
  var dta =await GetTechnialAbility();
  return (
    <div className="flex min-h-full flex-col items-center justify-between p-50">
      <div className="grid grid-cols-2 gap-5 sm:grid-cols-3">
        {dta.length > 0 &&

          dta.map((r, i) => {
            return <TwoSectionCard key={r.img} image={r.img} heading={r.heading} subheading={r.subheading} icon={r.icon} />
          })

        }
      </div>
    </div>

  )
}