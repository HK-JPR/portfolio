//"use client"; // This is a client component 👈🏽

import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { SpeedInsights } from "@vercel/speed-insights/next"
import { Analytics } from '@vercel/analytics/react';
import AllSideBar from "./layout/sidebarComb";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Hemant Koolwal",
  description: "",
};

export default function RootLayout({ children, }: Readonly<{ children: React.ReactNode; }>) {
  return (
    <html lang="en">
      <head>
        <link rel="icon" href="/favicon.ico" sizes="any" />
        <title>Hemant Koolwal</title>
        <meta property="description" content="Experienced IT professional with a proven track record in website development." />
        <meta property="og:title" content="Meet Hemant Koolwal: Software developer 💻 | Passionate about tech 📱 | Enthusiastic about innovation 🌟"></meta>
        <meta property="og:image" content="https://hemant-koolwal.vercel.app/logo.png"></meta>
        <meta property="og:description" content="Experienced IT professional with a proven track record in website development." />
        <meta property="og:url" content="https://hemant-koolwal.vercel.app"></meta>

        <meta property="twitter:card" content="summary"></meta>
        <meta property="twitter:site" content="@hemantkoolwal"></meta>
        <meta property="twitter:image" content="https://hemant-koolwal.vercel.app/logo.png"></meta>
        <meta property="twitter:title" content="Hemant Koolwal"></meta>
        <meta property="twitter:description" content="Experienced IT professional with a proven track record in website development"></meta>
      </head>
      <body className={inter.className}>
        <div className="grid min-h-screen grid-cols-[auto_1fr] justify-center gap-4 overflow-hidden p-4">
          <AllSideBar />
          <div className="h-[calc(100vh_-_2rem)] w-full">
            <main className="h-[calc(100vh_-_2rem)] w-full bg-slate-900 p-4 overflow-y-auto">
              {children}
              <Analytics />
              <SpeedInsights />
            </main>
          </div>
        </div>
      </body>
    </html>
  );
}
