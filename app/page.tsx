import Link from "next/link"
import SocialMedia from "./components/social-icon";

export default function Home() {
  return (
    <div className="flex min-h-full flex-col items-center justify-between p-20">
      <div className="z-10 max-w-5xl w-full items-center justify-between font-mono text-sm lg:flex">
        <h1 className="text-3xl">
          Hello,<br />
          This is <span className="text-theme animate-pulse"> Hemant Koolwal</span> , I&apos;m a Professional <span className="bg-gradient-to-r from-sky-500 to-indigo-500 px-2">Software Developer</span>.
        </h1>
      </div>

      <div>
        <p className="leading-7 text-white/70 antialiased">
          Experienced IT professional with a proven track record in website development. Adept at leveraging innovative technologies to deliver high-quality solutions that meet client needs. Strong problem-solving skills and a collaborative mindset, dedicated to driving business success through strategic IT initiatives. Passionate about staying updated with the latest trends in the industry to provide cutting-edge solutions.
        </p>
      </div>

      <div className="w-full">
        <SocialMedia />
      </div>


      <div className="mb-32 grid text-center lg:max-w-5xl lg:w-full lg:mb-0 lg:grid-cols-4 lg:text-left">
        <Link href="/get-in-touch" className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30">

          <h2 className={`mb-3 text-2xl font-semibold`}>
            Get In Touch{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
        </Link>
      </div>
    </div>
  );
}
