import { FaLinkedin, FaGithub, FaFacebook, FaInstagram,FaTelegram } from "react-icons/fa"
import { FaSquareXTwitter } from "react-icons/fa6";
export default function SocialMedia() {
    const aClassN = "text-theme hover:text-white hover:scale-150"
    return (
        <div className="my-5 flex items-center gap-5 lg:gap-10 text-3xl">
            <a href="https://www.linkedin.com/in/hemant-koolwal/" target="_blank" className={`${aClassN}`}>
                <FaLinkedin />
            </a>
            <a href="https://github.com/HK-JPR" target="_blank" className={`${aClassN}`}>
                <FaGithub />
            </a>
            <a href="https://www.facebook.com/hemant.koolwal.5" target="_blank" className={`${aClassN}`}>
                <FaFacebook />
            </a>
            <a href="https://www.instagram.com/hemant_koolwal/" target="_blank" className={`${aClassN}`}>
                <FaInstagram />
            </a>
            <a href="https://x.com/HemantKoolwal" target="_blank" className={`${aClassN}`}>
                <FaSquareXTwitter />
            </a>
            <a href="https://t.me/hemantkoolwal" target="_blank" className={`${aClassN}`}>
                <FaTelegram  />
            </a>
        </div>
    )
}