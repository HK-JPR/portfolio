import Image from "next/image"

export default function TwoSectionCard({ image, heading, subheading, icon }) {
  return (
    <div className="flex flex-col items-center bg-white border border-gray-200 rounded-lg shadow md:flex-row md:max-w-xl hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 transform transition duration-500 hover:scale-110">
      {
        GetVisualtEle(image, icon)
      }

      <div className="flex flex-col justify-between p-4 leading-normal">
        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{heading}</h5>
        <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">{subheading}</p>
      </div>
    </div>
  )
}


function GetVisualtEle(image, icon) {
  if (image) {
    return <Image src={image} width={100} height={100} className="p-4" alt={image} />
  } else {
    return icon 
  }
}