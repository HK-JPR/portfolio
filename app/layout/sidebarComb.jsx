"use client";
import Sidebar from "./sidebar"
import MobileSidebar from "./sidebarmobile"
import { useState } from "react";

export default function AllSideBar() {
    const [showSidebar, setShowSidebar] = useState(false);
    return (
        <>
            <MobileSidebar setter={setShowSidebar} />
            <Sidebar show={showSidebar} setter={setShowSidebar} />
        </>
    )
}
