import React from 'react'
import Link from 'next/link'
import logo from '@/public/logo.png'
import { FiMenu as Icon } from 'react-icons/fi'
import Image from 'next/image'

export default function MobileSidebar({ setter }) {
    return (
        <nav className="md:hidden z-20 fixed top-0 left-0 right-0 h-[60px] bg-black flex [&>*]:my-auto px-2">
            <button
                className="text-4xl flex text-white"
                onClick={() => {
                    setter(oldVal => !oldVal);
                }}
            >
                <Icon />
            </button>
            <Link href="/" className="mx-auto">
                {/*eslint-disable-next-line*/}
                <Image
                    src={logo.src}
                    alt="Logo"
                    width={50}
                    height={50}
                />
            </Link>
        </nav>
    )
}