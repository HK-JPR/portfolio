import { FaComputer } from "react-icons/fa6";
import { PiChartLineUpThin } from "react-icons/pi";

async function GetWorkHistory() {
  const myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");
myHeaders.append("Access-Control-Request-Headers", "*");
myHeaders.append("run_as_user_id", "660fd2558e68279dc6823674");

const raw = JSON.stringify({
  "collection": "WorkHistory",
  "database": "MyApp",
  "dataSource": "Cluster0"
});

const requestOptions = {
  method: "POST",
  headers: myHeaders,
  body: raw,
  redirect: "follow"
};

return fetch("https://ap-south-1.aws.data.mongodb-api.com/app/data-yvqtb/endpoint/data/v1/action/find", requestOptions)
  .then((response) => response.text())
  .then((result) => JSON.parse(result).documents)
  .catch((error) => console.error(error));
}

export default async function WorkHistory() {


  const iconClass = "h-5 w-5 text-theme"
  function getCardClass(index) {
    if (index % 2 == 1) {
      return { class1: "flex justify-end w-full mx-auto items-center", class2: "w-full sm:w-1/2 sm:pl-8" }
    } else {
      return { class1: "flex justify-start w-full mx-auto items-center", class2: "w-full sm:w-1/2 sm:pr-8" }
    }
  }

  let workHistory = await GetWorkHistory();
  return (
    <div>
      <div className="py-3 sm:max-w-xl sm:mx-auto w-full px-2 sm:px-0 ">
        <div className="relative text-gray-700 antialiased text-sm font-semibold">
          <div className="hidden sm:block w-1 bg-theme absolute h-full left-1/2 transform -translate-x-1/2">
          </div>
          {workHistory.length > 0 &&
            workHistory.reverse().map((r, i) => {
              return (
                <div key={i} className="mt-6 sm:mt-0 sm:mb-12">
                  <div className="flex flex-col sm:flex-row items-center">
                    <div className={getCardClass(i).class1}>
                      <div className={getCardClass(i).class2}>
                        <div className="p-4 bg-white rounded shadow border-theme border-1">
                          {r.Firm}  <span className="float-end text-gray-500">{r.Position}</span>
                          <span><br /> {r.From} - {r.To}</span>
                        </div>
                      </div>
                    </div>
                    <div className="rounded-full bg-white border-white border-4 w-8 h-8 absolute left-1/2 -translate-y-4 sm:translate-y-0 transform -translate-x-1/2 flex items-center justify-center">
                      {r.Icon}
                    </div>
                  </div>
                </div>
              )
            }) 
          }
        </div>

      </div>
    </div>
  )
}