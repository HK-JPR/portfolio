"use client";

import SendMessage from "../components/SendTeleMessage";
import SocialMedia from "../components/social-icon"
import { FormEvent } from 'react'




export default function GetInTouch() {
  async function onSubmit(event) {
    event.preventDefault()
    
    const formData = new FormData(event.currentTarget)
    // const response = await fetch('/api/submit', {
    //   method: 'POST',
    //   body: formData,
    // })

    // // Handle response if necessary
    // const data = await response.json()

    const values = {};
    for (const [name, value] of formData.entries()) {
      values[name] = value;
    }

    console.log(values);

    if (!values.Name && !values.Message && !values.Email) {
      return;
    }

    var message = `Fullname: ${values.Name} | Email: ${values.Email} | Message: ${values.Message}`;
    SendMessage(message);

    event.currentTarget.reset()
  }
  return (
    <div className="grid gap-5 sm:grid-cols-2 p-4">
      <div>
        <form className="w-full max-w-lg bg-gray-950 p-4 lg:p-8" onSubmit={onSubmit}>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3 md:mb-0">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="name">
                Name
              </label>
              <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="name" type="text" placeholder="Jane" name="Name" />
              {/* <p className="text-red-500 text-xs italic">Please fill out this field.</p> */}
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="email">
                Email
              </label>
              <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="email" type="email" placeholder="example@example.com" name="Email" />
              {/* <p className="text-gray-600 text-xs italic">Make it as long and as crazy as you'd like</p> */}
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <div className="w-full px-3">
              <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="message">
                Message
              </label>
              <textarea className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="message" type="password" placeholder="example@example.com" name="Message"></textarea>
              {/* <input  /> */}
              {/* <p className="text-gray-600 text-xs italic">Make it as long and as crazy as you'd like</p> */}
            </div>
          </div>
          <div className="flex flex-wrap -mx-3 mb-6">
            <button type="submit" className="bg-theme-dark hover:rotate-2 text-white font-bold py-2 px-4 rounded">Get In Touch</button>
          </div>
        </form>
      </div>
      <div>
        <SocialMedia className="justify-center" />
      </div>
    </div>
  )
}