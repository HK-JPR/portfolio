async function GetDbData() {
  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  myHeaders.append("Access-Control-Request-Headers", "*");
  myHeaders.append("run_as_user_id", "660fd2558e68279dc6823674");

  const raw = JSON.stringify({
    "collection": "IdentitySpecifics",
    "database": "MyApp",
    "dataSource": "Cluster0"
  });

  const requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow"
  };

  return fetch("https://ap-south-1.aws.data.mongodb-api.com/app/data-yvqtb/endpoint/data/v1/action/find", requestOptions)
    .then((response) => response.text())
    .then((result) => JSON.parse(result).documents[0].Data)
    .catch((error) => console.error(error));
}


export default async function IdentitySpecific() {

  let data = await GetDbData();
  return (
    <div className="flex min-h-full flex-col items-center justify-between p-50">
      <div className="z-10 max-w-5xl w-full items-center justify-between text-sm lg:flex">
        <div className="w-2/4"></div>
        <div className="w-2/4">

          {data.length > 0 &&

            data.map((element, i) => {
              if (i != 0) {
                return <> <hr key={i} className="w-48 h-1 mx-auto my-3 bg-gray-100 border-0 rounded dark:bg-gray-700" /> <p key={i} className="leading-7">{element}</p></>
              } else {
                return <p className="leading-7" key={i}>{element}</p>
              }
            })
          }
        </div>
      </div>
    </div>
  )
}